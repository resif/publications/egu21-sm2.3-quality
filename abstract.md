# Improving data, metadata and service quality within Résif-SI

# Abstract

Résif, the French seismological and geodetic network, was launched in 2009 in an effort to develop, modernize, and centralize geophysical observation of the Earth’s interior. This French research infrastructure uses both permanent and mobile instrument networks for continuous seismological, geodetic and gravimetric measurements.

Résif-SI is the Information System that manages, validates and distributes seismological data from Résif.

The construction of Résif-SI has lead to a federated organisation gathering several data and metadata producers (Nodes) and a national Seismological Data Centre.

The Résif Seismological Data Centre is one of 19 global centres distributing data and metadata in formats and using protocols which comply with International Federation of Digital Seismograph Networks (FDSN) standards. It is also one of the eleven nodes in EIDA, the European virtual data centre and seismic data portal in the European Plate Observing System (EPOS) framework.

Inside Résif-SI, each Node has it's specificities and dedicated procedures in order to manage and validate the data and metadata workflow from the station instruments to the Résif Seismological Data Centre.

To meet the expectations and needs of the end user in terms of data quality, metadata consistency and service availability, Résif-SI operates a complex set of quality enhancement operations.

This contribution will present the quality expectations that are in the core of Résif-SI, and show the methods and tools that help us meeting the expectations, and that could be of interest for the rest of the community.

We will then list some of our quality improvement projets and the expected results.

Covered topics :
- Metadata edition and validation
  - AROL / GATITO
  - YASMINE
  - PDF 
  - gresp
- Data validation and workflow management
  - Data quality criterions
  - Morumotto
  - Data availability
- Realtime data flow supervision
  - synapse
  - sl2influx
  - Résif-DC inner monitoring
- User support

# Notes internes

## Guidelines

https://egu21.eu/guidelines/presenter_guidelines.html

  - Introduction de 2 minutes. 1 slide au format PNG 150dpi, 16:9. À soumettre 24h avant la session
  - Suivi d'un chat 🐈 
  - Matériel de présentation supplémentaire : format PDF limité à 50MB

## Contenu

Présenter l'ensemble des projets permettant d'améliorer la qualité de la donnée, de la métadonnée et des services qu'on met en œuvre au sein de Résif-SI (qui sont souvent pas encore terminés dans la plupart des exemples qui me viennent à l'esprit). On prend l'angle de la qualité, de l'amélioration continue et on y va en attaquant sous les angles :

- Gestion de la métadonnée (rédaction et soumission)
- Gestion de la donnée (flux de données, processus de qualification)
- Gestion des services (distribution des données, métadonnées, evénements, produits dérivés, disponibilité, etc.)

Pour chacun, on peut dérouler :
* un état des lieux (enjeux et difficultés)
* les projets d'amélioration
  - métadonnée : Yasmine, AROL, GATITO, MARUMOTO, GISMO, statistiques, homogénéisation pour la distribution, DOI, obsinfo
  - donnée : DMP Résif-SI, surveillance synapse, statistiques, archivage déporté
  - services : monitoring, statistiques,
* les résultats (attendus ou déjà obtenus)

Le plan peut se faire au choix dans les 2 axes.

### Gestion de la métadonnée

### Gestion de la donnée

### Gestion des services
#### État des lieux et enjeux
Des services pour le SI fournis par le centre de données.
#### Projets d'amélioration
#### Résultats attendus ou obtenus


# Poster de présentation
Diviser le poster de présentation en 4 parties

