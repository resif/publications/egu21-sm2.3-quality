\documentclass[10pt,aspectratio=169]{beamer}
\usepackage{fontspec}
\setmainfont{Latin Modern Sans}
\setmonofont{Source Code Pro}
\usepackage{tikz}
\usepackage{resif}
\usepackage{xcolor}
\usepackage[type={CC}, modifier={by}, version={4.0}]{doclicense}
\usepackage{hyperxmp}
\usepackage{minted}

\usepackage{natbib}
\bibliographystyle{apalike}
% make bibliography entries smaller
\renewcommand\bibfont{\scriptsize}
% If you have more than one page of references, you want to tell beamer
% to put the continuation section label from the second slide onwards
\setbeamertemplate{frametitle continuation}[from second]
% Now get rid of all the colours
\setbeamercolor*{bibliography entry title}{fg=black}
\setbeamercolor*{bibliography entry author}{fg=black}
\setbeamercolor*{bibliography entry location}{fg=black}
\setbeamercolor*{bibliography entry note}{fg=black}
% and kill the abominable icon
\setbeamertemplate{bibliography item}{}
\setbeamertemplate{footline}[frame number]
\setbeamertemplate{navigation symbols}{}
\titlegraphic{\includegraphics[height=1cm]{resif.png}~~~\includegraphics[height=1cm]{egu.png}\\{\tiny \url{https://www.resif.fr}\\DOI:~\url{https://doi.org/10.5194/egusphere-egu21-12436}}\\\doclicenseIcon}
% \setbeamertemplate{background}{%
% \begin{tikzpicture}[remember picture,overlay]
% \node[anchor=south west,yshift=10pt] at (current page.south west) {\includegraphics[height=1cm]{resif.png}};
% \end{tikzpicture}}

\title{Improving data, metadata and service quality within Résif-Epos}
\author{Jonathan Schaeffer, for the technical committee of Résif seismology}
\date{28/04/2021\\\href{https://meetingorganizer.copernicus.org/EGU21/EGU21-12436.html}{EGU vPico SM2.3}}

\AtBeginSection[]{\begin{frame}\frametitle{}\LARGE\insertsectionhead\vspace{0.1cm}\hrule\end{frame}}


\begin{document}

\begin{frame}
	\titlepage
\end{frame}

\section{Context}
\begin{frame}{Résif-SI for seismology organisation}
\begin{figure}
\centering
\includegraphics[width=.9\textwidth]{figures/figure02.pdf}
\caption{A-Nodes, supported by national institutions, are responsible for producing raw and validated data, and submitting metadata to the B-node (aka Résif-DC). Résif-DC is responsible for hosting safely and distributing (meta)data and to ensure interoperability within EIDA and Epos}\label{fig:resifsi}
\end{figure}
\end{frame}

\section{Metadata Management Quality}
\begin{frame}{Metadata Service Quality}
\begin{block}{}
\textbf{Goal}: Distribute uniform metadata to the users
\end{block}
\begin{itemize}
\item Metadata in FDSN stationXML format produced and validated by the Networks (A-Nodes)
\item since 2020, metadata submitted to B-Node in StationXML v1.1 format:
\begin{itemize}
\item Metadata by station
\item Validate to FDSN stationXML version 1.1 XSD
\item Validate with IRIS-DMC stationxml-validator v1.7.1
\end{itemize}
\item Common understanding for some elements descriptions
\begin{itemize}
\item No personal email in contacts
\item Uniform institutes presentation
\item ...
\end{itemize}
\item Still not completely uniform => Need for a common library
\end{itemize}
\end{frame}

\begin{frame}{Common metadata objects library (GATITO)}
\begin{block}{}
\textbf{Goal}: Define within Résif-SI a common description of some metadata, for all metadata editors~\citep{gatito}.
\end{block}
\begin{itemize}
  \item Define common metadata objects within Résif-SI :
  \begin{itemize}
    \item Agencies
    \item Units
    \item Geology
    \item Vault
    \item Channel comments
    \item Identifiers
  \end{itemize}
  \item Plain YAML files
  \item Designed and used by Yasmine within Résif-SI for metadata edition but can be used outside of Yasmine too
\end{itemize}
\end{frame}

\subsection{Instruments metadata management}
\begin{frame}[fragile]{AROL: The Atomic Response Objects Library}
\begin{block}{}
\textbf{Goal}: Build an instrument response library, complementary to NRL, defining responses for datalogger and sensors.
\end{block}
AROL~\citep{arol} aims to build and distribute a comprehensive library of instruments with their caracteristics to ease instrumental response composition.

Support any type of instrument, including calibrated instruments.

\begin{figure}[ht]
  \centering
  \includegraphics[width=.8\textwidth]{figures/arol_yasmine.pdf}
  \caption{Workflow for the creation of an instrumental response using AROL as input with Yasmine\label{fig:arol_yasmine} }
\end{figure}

\end{frame}

\begin{frame}[fragile,shrink=15]{AROL structure}
  \begin{columns}
\begin{column}[t]{.47\columnwidth}
  \begin{block}{AROL dataloggers tree}
    { \scriptsize
    \begin{verbatim}
└── Dataloggers
    ├── fairfield
    │   ├── FAIRFIELD.1000.response.yaml
    │   ├── ...
    │   ├── fairfield.yaml
    │   └── include
    │       ├── fir_zland_1.filter.yaml
    │       └── ...
    ├── nanometrics
    ├── reftek
    └── ...
    \end{verbatim}
      }
  \end{block}
\end{column}
\begin{column}[t]{.47\columnwidth}
  \begin{block}{AROL sensors tree}
    { \scriptsize
    \begin{verbatim}
└── Sensors
    ├── fairfield
    │   ├── fairfield.yaml
    │   ├── include
    │   │   ├── zland3C.pz.filter.yaml
    │   │   └── zland.pz.filter.yaml
    │   ├── ZLAND3C.response.yaml
    │   └── ZLAND.response.yaml
    ├── guralp
    │   ├── CMG3E.response.yaml
    │   ├── ...
    │   ├── guralp.yaml
    │   └── include
    │       ├── CMG_100.pz.filter.yaml
    │       └── ...
    └── ...
    \end{verbatim}
      }
  \end{block}
\end{column}
\end{columns}
{
\centering Navigate the library at \url{https://gitlab.com/resif/arol}
}
\end{frame}

\begin{frame}{AROL: Yasmine integration}
  \begin{columns}
    \begin{column}[t]{.47\columnwidth}
     \begin{figure}[ht]
       \centering
       \includegraphics[width=.8\textwidth]{figures/arol_yasmine_datalogger.png}
       \caption{\label{fig:arol_yasmine_datalogger} Yasmine presents an interface in to select a datalogger from AROL library}
     \end{figure}
      \end{column}
    \begin{column}[t]{.47\columnwidth}
     \begin{figure}[ht]
       \centering
       \includegraphics[width=.7\textwidth]{figures/arol_yasmine_sensor.png}
       \caption{\label{fig:arol_yasmine_datalogger} Yasmine presents an interface in to select a sensor from AROL library}
     \end{figure}
      \end{column}
  \end{columns}
\end{frame}

\begin{frame}{AROL: Yasmine integration}
  \begin{columns}
    \begin{column}[t]{.47\columnwidth}
     \begin{figure}[ht]
       \centering
       \includegraphics[width=.8\textwidth]{figures/arol_yasmine_response.png}
       \caption{\label{fig:arol_yasmine_datalogger} Yasmine presents the channel response}
     \end{figure}
      \end{column}
    \begin{column}[t]{.47\columnwidth}
     \begin{figure}[ht]
       \centering
       \includegraphics[width=.7\textwidth]{figures/arol_yasmine_response_plot.png}
       \caption{\label{fig:arol_yasmine_datalogger} Yasmine shows a plot of the response}
     \end{figure}
      \end{column}
  \end{columns}
\end{frame}

\section{Data management quality}
% Qualité de la gestion de la donnée (flux de données, processus de qualification) flux de données validées et flux des données temps réel. Vérification du format miniseed des fichiers. Toute donnée doit avoir une métadonnée en base. Critères FAIR , Certification CoreTrustSeal en cours
\begin{frame}{Data quality flow}
\begin{figure}
\begin{minipage}[c]{0.67\textwidth}
\includegraphics[width=\textwidth]{figures/data_workflow.pdf}
\end{minipage}\hfill
\begin{minipage}[c]{0.3\textwidth}
\caption{Raw data is gathered and distributed by Résif-DC only if valid metadata exists. This endpoint is used as a source to store raw data in the archive and made available in near real time through fdsnws-dataselect. As validated data is ingested, corresponding raw data is removed from archive}\label{fig:dataflow}
\end{minipage}
\end{figure}
\end{frame}

\begin{frame}{Data availability by quality}
The fdsnws-availability webservice shows data available at Résif-DC
% - illustration par les graphs d'availability par l'outil de Philippe
\begin{figure}
\centering
\includegraphics[width=\textwidth]{figures/availability_exemple.png}
\caption{fdsnws-availabilty provides finegrained information on data availability by quality. This exemple from network RA shows that validated data is completed by recent raw data.}\label{fig:availability}
\end{figure}
\end{frame}

\begin{frame}{Validated data checks}
\begin{block}{}
\textbf{Goal}: Ensuring the cleanest possible data for the end user.
\end{block}
% - vérification du MiniSEED validé selon ce qu'on s'est accordé.
Several checks are made at node-B ensuring clean data ingestion :
\begin{itemize}
\item homogeneous miniSEED headers
\item homogeneous samplerate in the submitted data
\item blocks size (4096 bytes)
\item corresponding metadata exists
\item data timerange is consistant with the metadata
\end{itemize}
\end{frame}

\begin{frame}[shrink=15]{FAIR data {\scriptsize as described by \url{https://www.go-fair.org/fair-principles/}}}
\begin{columns}
\begin{column}[t]{.47\columnwidth}
\begin{block}{1. Findable}
\begin{itemize}
\item[F1] DOI by network, landingpage presenting information from stationXML metadata
\item[F2] stationXML metadata is rich and it's quality is ensured.
\item[F3] DOI mentioned in stationXML metadata 
\item[F4] eidaws-federator advertises Resif-DC metadata ; Networks are registered at FDSN along with their DOI ; Epos integration
\end{itemize}
\end{block}
\begin{block}{4. Reusable}
\begin{itemize}
\item[R1] Metadata contains rich information on how to use the data
\item[R1] Data is licensed as \doclicenseNameRef \doclicenseIcon
\end{itemize}
\end{block}
\end{column}

\begin{column}[t]{.47\columnwidth}
\begin{block}{2. Accessible}
\begin{itemize}
\item[A1] (meta)data distributed through FDSN standard webservices (local and EIDA token authentication supported)
\item[A2] Metadata remains even if data is removed
\end{itemize}
\end{block}
\begin{block}{3. Interoperable (almost)}
\begin{itemize}
\item[I1] Metadata and data published in standard FDSN formats
\item[I2] StationXML format is well documented, though a standard thesaurus for seismology does not exist as for now.
\item[I3] not yet
\end{itemize}
\end{block}
\end{column}
\end{columns}

\end{frame}

\section{Services and software management}
\subsection{Services quality management}
% \begin{frame}{Methodology}
% % Expliquer la démarche de qualité de service.
% % Un service => Un indicateur => Un point annuel

% While not following formal specifications of service quality management, we strive to drive the services quality by a management board (Résif-CoPil).

% Management tools : monitoring, yearly metrics, users feedbacks.
% \end{frame}
\begin{frame}{Monitoring: From error detection to pro-active alerting}

Resif-DC implements systematic monitoring at several levels :
\begin{itemize}

\item[✓] Low level errors detection (disk failures, servers downtime, system overloads, ...)
\item[✔] Service errors detection (is the service available ?)
\item[✔] Services functions supervision (does the service reply correctly to simple requests ?)
\item[✔] Some proactive alerting ("disk volume will be full in XX hours")
\item[✔] Service availability reports
\end{itemize}
Scheduled downtimes are published on the data portal \url{https://seismology.resif.fr}
\end{frame}


\begin{frame}{From statistics to service metrics}
In order to evaluate the services usage, the management board needs some metrics that can be evaluated from one year to next.
\begin{figure}
  \begin{minipage}[c]{0.67\textwidth}
    \includegraphics[width=\textwidth]{figures/logging.pdf}
  \end{minipage}\hfill
  \begin{minipage}[c]{0.3\textwidth}
    \caption{Usage events are collected from different sources, compiled in statistics by analyzers and stored in databases. Finally a yearly report \citep{resifdc-report} (sources : \citep{resifdc-report-sources}), a publicly available statistics webservice \citep{wsstatistics} and a graphs and plotting tool \citep{resif-delivery-stat-plotter} are created from those statistics.}\label{fig:stats}
  \end{minipage}
\end{figure}
\end{frame}

\begin{frame}{Metrics illustration}
\begin{figure}
  \begin{minipage}[c]{0.7\textwidth}
\includegraphics[width=0.92\textwidth]{figures/indicateurs_dataholdings_cumul.png}
  \end{minipage}\hfill
  \begin{minipage}[c]{0.3\textwidth}
\caption{\scriptsize Twice a year, the archive is scanned in order to count the data held, by data type, by network, by station and by channel. The resulting database allows to extract rich information showing current holding but also the evolution in time.}\label{fig:indic_dataholdings}
\end{minipage}
\end{figure}
\begin{figure}
  \begin{minipage}[c]{0.7\textwidth}
  \includegraphics[width=0.92\textwidth]{figures/indicateurs_dataselect_req.png}
  \end{minipage}\hfill
  \begin{minipage}[c]{0.3\textwidth}
    \caption{\scriptsize Each HTTP request is logged in the ELK stack. The request is analyzed and the longterm statistics are stored in a postgresql database. Metrics start from 2014 on.}\label{fig:indic_dataselect}
  \end{minipage}
\end{figure}
\end{frame}

\begin{frame}{Impact on service quality}
\begin{itemize}
\item too early to show formal results
\item but conception of services, with quality in mind, tends to better implementations, maintainability and service lifecycle management
\item accurate and reproductible metrics provide management tool for deciders, compiled in a yearly report.
\end{itemize}
\end{frame}

\subsection{Software management}

\begin{frame}{Software quality: Common practices shared within Résif-SI}

\begin{block}{Open and free (as in speech) source codes}
\begin{itemize}
\item released under GPLv3
\item publicly versionned and documented on \url{https://gitlab.com/resif}
\end{itemize}
\end{block}

\begin{block}{Same development workflow for all webservices}
\begin{itemize}
\item Automated validation (To not break the production)
\item Continuous integration (Gitlab-CI)
\item Towards continuous deployment (Kubernetes)
\end{itemize}
\end{block}

\begin{block}{Software designed to ease reusability}
\begin{itemize}
\item i.e. WS-Availability by EIDA based on RESIF's implementation
\item Welcome external contributions through issues submission or pull requests
\end{itemize}
\end{block}
\end{frame}

\section{Conclusion}
\begin{frame}{Conclusion}
\begin{block}{Who's happy now?}
\begin{itemize}
\item Résif-SI operators: Clear workflow and responsibilities makes new nodes integration easier (i.e. Node Marine)
\item Researchers: access to uniform and rich metadata content, clean data, reliable services
\item Principal investigators: usefull statistics, FAIR data distributed
\end{itemize}
\end{block}
\begin{block}{Continuous Improvement: what's next}
\begin{itemize}
\item Résif-DC CoreTrustSeal certification
\item Estimating and lowering Résif-DC carbon footprint
\item Yasmine + AROL + Gatito for all A-nodes
\end{itemize}
\end{block}
\end{frame}


\begin{frame}{Authors {\scriptsize the technical committee of Resif-SI for seismology }}
\begin{itemize}
\item Constanza Pardo [Head of technical committee] (Institut de Physique du Globe, CNRS)
\item Philippe Bollard (Observatoire des Sciences de l'Univers de Grenoble, CNRS)
\item Wayne Crawford (Institut de Physique du Globe, CNRS)
\item Fabien Engels (Ecole et Observatoire des Sciences de la Terre)
\item Christophe Maron (Observatoire Côtes d'Azure)
\item Marc Grunberg (Ecole et Observatoire des Sciences de la Terre)
\item Jean-Marie Saurel (Institut de Physique du Globe, CNRS)
\item Jonathan Schaeffer (Observatoire des Sciences de l'Univers de Grenoble, CNRS)
\item David Wolyniec (Observatoire des Sciences de l'Univers de Grenoble, CNRS)
\end{itemize}
\end{frame}
\begin{frame}[t,allowframebreaks]
\frametitle{References}
\renewcommand{\refname}{References}
\bibliography{refs}
\end{frame}
\end{document}

% Le cadre présenté facilite l'intégration d'un nouveau nœud A dans Résif-SI => obsinfo pour la gestion des métadonnées
% Intégration dans EPOS => Qualité reconnue ?
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% TeX-command-extra-options: "-shell-escape"
%%% End:
